from unittest import case

import pandas as pd
import os
import tools
import numpy
import json

class checkfiles():
    def __init__(self, dir, listFiles):
        self.files = listFiles
        self.dir = dir
        self.result = []
        self.result.append("\nRESULTS: ")



    def checkfilebasics(self):
        for f in self.files:
            print("\n***** ", f)
            file = self.dir + "/" + f
            dfSheets = pd.read_excel(file, None);

            if "Struktur" in f or ("Instanz" in f):
                for shts in dfSheets.keys():
                    if shts[0] != '#':
                        excel_data_df = pd.read_excel(file, sheet_name=shts, engine='openpyxl')
                        number_of_rows = len(excel_data_df.index)
                        number_of_cols = len(excel_data_df.columns)
                        list_of_cols = excel_data_df.columns.values.tolist()
                        print("\n", shts, "  nr of ROWS:", number_of_rows, "  nr of COLS:", number_of_cols)

                        matrix = []
                        for col in list(excel_data_df.columns):
                            rows = []
                            for i in range(number_of_rows):
                                if pd.isna(excel_data_df[col][i]):
                                    rows.append(0)
                                else:
                                    rows.append(1)
                            matrix.append(rows)

                            colsToCheck= ["Key", "Type"]
                            if "Pfad" in col:
                                colvar = "Pfad"
                            else:
                                colvar = col

                            if colvar in colsToCheck:
                                r = tools.checkCols(col, excel_data_df[col])
                                if r:
                                    self.result.append("Check colname: " + col + " in File:" + f + " sheets: " + shts)
                                    self.result.append(r)

                        # Compare matrix01
                        mdf = pd.DataFrame(matrix)
                        matrix = mdf.T
                        #col_names_old = matrix.columns.values.tolist()
                        m = matrix.set_axis(list_of_cols, axis=1)

                        r = tools.compareMatrix01(m, number_of_rows, number_of_cols)
                        if r:
                            self.result.append("File:" + f + " sheets: " + shts)
                            self.result.append(r)


                    #secondcol = []
                    #if matrix:
                    #    secondcol = []
                    #    for i in range(len(matrix)):
                    #        if matrix[i][0] == 0:
                    #            self.result.append("Fatal ERROR - empty cell in first column - file:" + f + "  sheet:" + shts + " row:" + str(i + 1))
                    #            tools.reports(self.result)
                    #            exit()
                    #        if matrix[i][1] == 0:
                    #            sc = []
                    #            sc.append("empty cells found in second column")
                    #            sc.append(f)
                    #            sc.append(shts)
                    #            sc.append(i+1)
                    #            secondcol.append(sc)

        return(self.result)


def getFilesList(tierart):
    iktpath = "data/inkalktier/Beschreibung/Tierart"
    files = os.listdir(iktpath)
    list = []
    i = 1
    for f in files:
        if tierart == f.split('_', 1)[1]:
            iktpath = iktpath + "/" + f + "/Produktionsrichtung/1_Haltung"
            print("neue path: ", iktpath)
            for ff in os.listdir(iktpath):
                list.append(ff)
    return list

def path_to_dict(path):
    print(path)
    d = {'name': os.path.basename(path)}
    if os.path.isdir(path):
        d['type'] = "directory"
        d['children'] = [path_to_dict(os.path.join(path,x)) for x in os.listdir\
            (path)]
    else:
        d['type'] = "file"
        print()
    return d




