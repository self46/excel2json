from flask import Flask, jsonify, request, render_template
import buildJson

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False   


@app.route('/', methods=['GET'] )
def init():
    
    return render_template('base.html')


# call function to build the Json-string, get from url the values for sheetname
@app.route('/test/<string:sname>', methods=['GET'])
def newJson(sname):
    list = buildJson.readsheet("working-Excelsheet.xlsx", sname)

    return jsonify(list)

if __name__ == '__main__':
    # Linux
    #app.run(host='0.0.0.0')
    app.run(port=5000, debug=True)



